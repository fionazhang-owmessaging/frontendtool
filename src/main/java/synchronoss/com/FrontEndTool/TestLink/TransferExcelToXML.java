package synchronoss.com.FrontEndTool.TestLink;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.jdom2.CDATA;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class TransferExcelToXML {

  private final static String testSuite = "testsuite";
  private final static String testCase = "testcase";
  private final static String summary = "summary";
  private final static String precondition = "preconditions";
  private final static String expectedResults = "expectedresults";
  private final static String keyword = "keyword";
  private final static String step = "steps";

  // private final static String keywordsContentRegex = "(\\s+)|(\\,)";
  private final static String keywordsContentRegex = "\\,";
  private static final Logger LOGGER = Logger.getLogger(TransferExcelToXML.class.getName());

  public static void main(String[] args) {

    if (args.length != 3) {
      System.out.println(
          "Please input 3 parameters, first one is the excel absolutpath, second one is the sheet number, start from 1, thrid one is property file");
      System.exit(0);
    }

    String excelFile = args[0];
    String sheetNumberFromUser = args[1];
    String propertyFileLocation = args[2];


    // generate error folder
    File errorFolder = new File("error_log");
    generateFolder(errorFolder);

    // generate import xml folder
    File xmlFolder = new File("import_xml");
    generateFolder(xmlFolder);


    // get case excel folder path
    File caseFile = new File(excelFile);


    Workbook wb = null;
    int sheetNumber;
    int loopStartNumber = 2;
    FileOutputStream errorLogis = null;
    FileOutputStream errorLogSubject = null;
    try {
      // create file stream
      FileInputStream is = new FileInputStream(caseFile);
      File errorLogFileSubject = new File(
          errorFolder.getAbsoluteFile() + File.separator + "ErrorLog_SubjectOver100Char.txt");
      errorLogSubject = new FileOutputStream(errorLogFileSubject);

      wb = WorkbookFactory.create(is);

      if (sheetNumberFromUser.equals("all")) {
        sheetNumber = wb.getNumberOfSheets();
      } else {
        sheetNumber = Integer.parseInt(sheetNumberFromUser);
        loopStartNumber = sheetNumber;
      }

      // get the first sheet
      for (int i = loopStartNumber - 1; i < sheetNumber; i++) {
        Document document = new Document();
        document.setRootElement(new Element("testsuite"));

        File xmlFile2 =
            new File(xmlFolder.getAbsolutePath() + File.separator + wb.getSheetName(i) + ".xml");

        File errorLog = new File(errorFolder.getAbsoluteFile() + File.separator + "ErrorLog_"
            + wb.getSheetName(i) + ".txt");

        errorLogis = new FileOutputStream(errorLog);

        List<String> columnHeaders = new ArrayList<String>();
        System.out.println(
            "***** Starting to generate import xml of sheet: " + wb.getSheetName(i) + " *****");
        Sheet sheetNum = wb.getSheetAt(i);

        int countCaseNumber = 0;
        // iterator the first row
        for (Iterator<Row> rowsIT = sheetNum.rowIterator(); rowsIT.hasNext();) {
          Row row = rowsIT.next();
          boolean emptyRow = true;
          int index = 0;


          // Define root element of the xml
          Element firstParentElement = new Element("testsuite");
          Element secondParentElement = new Element("testcase");
          Element testStepsElement = new Element("steps");

          ArrayList<Element> stepList = new ArrayList<Element>();

          // Iterate each cells.
          for (Iterator<Cell> cellsIT = row.cellIterator(); cellsIT.hasNext();) {


            /*
             * if (checkIfMergeCell(sheetNum,index)){ index++; continue; }
             */
            emptyRow = false;
            Element childElement = null;
            Cell cell = cellsIT.next();
            cell.setCellType(Cell.CELL_TYPE_STRING);

            if (row.getRowNum() == 0) {
              columnHeaders.add(cell.getStringCellValue().trim());

            } else {
              // remove the white spaced of column header

              String elementTagName = columnHeaders.get(index).replaceAll("\\s+", "");

              // check with element tag
              switch (getProperties(elementTagName, propertyFileLocation)) {
                case summary:
                  elementTagName = "summary";
                  break;
                case precondition:
                  elementTagName = "preconditions";
                  break;
                case testSuite: { // folder
                  firstParentElement.setAttribute("name", cell.getStringCellValue());
                  document.getRootElement().addContent(firstParentElement);
                  index++;
                  continue;
                }
                case testCase: { // case name
                  if (cell.getStringCellValue().length() > 100) {
                    String errorInfo = "Subject in Sheet-" + wb.getSheetName(i) + " of line "
                        + (row.getRowNum() + 1) + " is over 100 characters \n";
                    errorLogSubject.write(errorInfo.getBytes());
                  }
                  secondParentElement.setAttribute("name", cell.getStringCellValue());
                  firstParentElement.addContent(secondParentElement);
                  index++;
                  continue;
                }
                case step: {
                  String stepContents = cell.getStringCellValue();

                  // split the test steps with return character and number
                  String stepContent[] = stepContents.split("(?=\n\\d)");

                  for (int j = 0; j < stepContent.length; j++) {
                    Element testStepElement = new Element("step");
                    Element stepNumberElement = new Element("step_number");
                    stepNumberElement.addContent(new CDATA(String.valueOf(j + 1)));
                    Element stepActionElement = new Element("actions");
                    stepActionElement.addContent(new CDATA(stepContent[j]));
                    testStepElement.addContent(stepNumberElement);
                    testStepElement.addContent(stepActionElement);
                    testStepsElement.addContent(testStepElement);
                    stepList.add(testStepElement);
                  }
                  secondParentElement.addContent(testStepsElement);

                  index++;
                  continue;
                }
                case expectedResults: {
                  int loopStepSize = stepList.size();
                  String expectedContents = cell.getStringCellValue();

                  // split the expected result with return character and number
                  String expectedContent[] = expectedContents.split("(?=\n\\d)");
                  if (expectedContent.length != stepList.size()) {
                    String errorInfo = "Sheet-" + wb.getSheetName(i)
                        + ": The 'steps' and 'expected result' number in line "
                        + (row.getRowNum() + 1) + " doesn't match \n";
                    errorLogis.write(errorInfo.getBytes());
                  }
                  for (int j = 0; j < expectedContent.length; j++) {
                    Element expectedResult = new Element("expectedresults");
                    expectedResult.addContent(new CDATA(expectedContent[j]));
                    if (j < loopStepSize) {
                      stepList.get(j).addContent(expectedResult);
                    } else {
                      Element testStepElement = new Element("step");
                      Element stepNumberElement = new Element("step_number");
                      stepNumberElement.addContent(new CDATA(String.valueOf(j + 1)));
                      Element stepActionElement = new Element("actions");
                      testStepElement.addContent(stepNumberElement);
                      testStepElement.addContent(stepActionElement);
                      testStepsElement.addContent(testStepElement);
                      stepList.add(testStepElement);
                      testStepElement.addContent(expectedResult);
                      stepList.add(testStepElement);
                    }
                  }
                  index++;
                  continue;
                }
                case keyword: {
                  String keywordsCollections = cell.getStringCellValue();
                  Element keywords = new Element("keywords");

                  String keywordsArray[] = keywordsCollections.split(keywordsContentRegex);

                  for (int j = 0; j < keywordsArray.length; j++) {
                    Element keyword = new Element("keyword");
                    keyword.setAttribute("name", keywordsArray[j]);
                    Element notes = new Element("notes");
                    notes.addContent(new CDATA(keywordsArray[j]));
                    keyword.addContent(notes);
                    keywords.addContent(keyword);
                  }
                  secondParentElement.addContent(keywords);
                }

                  index++;
                  continue;
                default: {
                }
              }

              // if the cell content is empty, escape current loop
              /*
               * if (cell.getStringCellValue().equals("")) { index++; continue; }
               */

              // add other element to second parent

              childElement = new Element(elementTagName);
              if (cell.getStringCellValue().equals("") || cell.getStringCellValue().length() == 0
                  || cell.getStringCellValue().equals("\n")
                  || cell.getStringCellValue().equals(" ")) {
                childElement.addContent("null");
              } else {
                childElement.addContent(new CDATA(cell.getStringCellValue()));
              }
              secondParentElement.addContent(childElement);

            }
            index++;
          }
          countCaseNumber++;
        }
        if (sheetNum.getLastRowNum() != (countCaseNumber - 1)) {
          System.out.println("[Error] the xml count is not equals to excel line count in sheet "
              + sheetNum.getSheetName());
          LOGGER.warning("The count is not correct!");
          LOGGER.info("countCaseNumber" + (countCaseNumber - 1));
          LOGGER.info("last row number" + sheetNum.getLastRowNum());

        } else {
          // LOGGER.info("Line number is correct");
        }

        // display with pretty format, and set encoding as UTF-8
        Format format = Format.getPrettyFormat();
        format.setEncoding("UTF-8");

        // if use the writeXML class, it will not convert to UTF-8
        XMLOutputter xmlOutput = new XMLOutputter(format);
        xmlOutput.output(document, new FileOutputStream(xmlFile2));
        System.out.println("Location: " + xmlFile2.getAbsolutePath());
        System.out.println("Done");
      }
      errorLogis.close();
      errorLogSubject.close();

      System.out.println("Subject over 100 character error log is generated at location: \n"
          + errorLogFileSubject.getAbsolutePath());
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InvalidFormatException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * Generate folder
   * 
   * @param file
   */
  public static void generateFolder(File file) {
    try {
      FileUtils.deleteDirectory(file);
      file.mkdir();
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
  }

  /**
   * Check if current cell is merged or not
   * 
   * @param sheet
   * @param index
   * @return
   */
  public static boolean checkIfMergeCell(Sheet sheet, int index) {
    // System.out.println("check if merged cell , the index is: " + index);
    boolean result = false;
    System.out.println("current index is : " + index);
    System.out.println(sheet.getNumMergedRegions());
    for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
      CellRangeAddress range = sheet.getMergedRegion(index);
      System.out.println("first row: " + range.getFirstRow());

    }
    System.out.print("index is : " + index);
    return result;
  }

  public static int getMergeCellNumber(Sheet sheet) {
    int count = 0;
    count = sheet.getNumMergedRegions();
    System.out.println("Totally merge line is : " + count);
    return count;
  }


  /**
   * Get the relationship between column
   * 
   * @param key
   * @return
   */
  public static String getProperties(String key, String propertyFileLocation) {
    String property = null;
    try {
      Properties propertyFile = new Properties();
      // System.out.println(System.getProperty("user.dir"));
      FileInputStream fis = new FileInputStream(propertyFileLocation);
      propertyFile.load(fis);
      property = (String) propertyFile.get(key);

    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return property;
  }

}

